from app.app import app
from requests import get, post

if __name__ == "__main__":
    app.run(port=5001)
